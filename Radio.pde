class Radio {
  int x, y;
  color c1, c2;
  int radius;
  int id;
  Radio[] radioGroup;
  boolean isChecked = false;
  
  Radio(int x, int y, int radius, color c1, color c2, int id, Radio[] rg) {
    this.x = x;
    this.y = y;
    this.radius = radius;
    this.c1 = c1;
    this.c2 = c2;
    this.id = id;
    this.radioGroup = rg;
  }
  
  boolean isPressed(int mx, int my) {
    if (dist(mx, my, x, y) < radius) {
      for (int i = 0; i < radioGroup.length; i++) {
        if (i != id) {
          radioGroup[i].isChecked = false;
        }
        else{
          radioGroup[i].isChecked = true;
        }
      }
      return true;
    }
    return false;
  }
  
  void display() {
    stroke(c1);
    fill(c1);
    ellipse(x, y, radius, radius);
    if (isChecked) {
      fill(c2);
      ellipse(x, y, radius, radius);
    }
    
  }
}
