import  ddf.minim.*;
import java.io.File;

Minim minim;
AudioInput in;
int gameMode = 0;

PImage bird1;
PImage bird2;
PImage bird3;
PImage bird4;
PImage bird5;

int characterNum = 1;

PImage bg;
PImage infoFg;
PImage base;
PImage gameover;
PImage title;
PImage mute_icon;
PImage sound_icon;

GameState gs;
PImage playButton1, playButton2;
Button startGame_playButton;
PImage infoButton1, infoButton2;
Button startGame_infoButton;
int startGame_w = 64;
int startGame_h = 32;
Button restartGame_playButton;
PImage backButton1, backButton2;
Button infoGame_backButton;
PImage menuButton1, menuButton2;
Button restartGame_menuButton;

Radio[] infoGame_radioGameModeButtons = new Radio[2];
Radio[] infoGame_radioCharacterSelectButtons = new Radio[5];

int pipe_size = 50;
Pipe pipe1;
Pipe pipe2;
Pipe pipe3;
Pipe pipe4;

Base b1;

int score = 0;
int highscore;
ArrayList<Pipe> pipes;
Bird bird;

PFont font;

boolean mute;
AudioPlayer player;
AudioPlayer point;
AudioPlayer hit;

PrintWriter output;



void setup() {
  size(500, 500);
  pipe1 = new Pipe(int(random(50, height - 50 - pipe_size * 3)), 500);
  pipe2 = new Pipe(int(random(50, height - 50 - pipe_size * 3)), 700);
  pipe3 = new Pipe(int(random(50, height - 50 - pipe_size * 3)), 900);
  pipe4 = new Pipe(int(random(50, height - 50 - pipe_size * 3)), 1100);
  pipes = new ArrayList<Pipe>();
  pipes.add(pipe1);
  pipes.add(pipe2);
  pipes.add(pipe3);
  pipes.add(pipe4);
  bird = new Bird(250, 0);
  
  bird1 = loadImage("bird.png");
  bird2 = loadImage("bird1.png");
  bird3 = loadImage("bird2.png");
  bird4 = loadImage("bird3.png");
  bird5 = loadImage("bird4.png");
  
  
  bg = loadImage("bg.png");
  title = loadImage("title.png");
  base = loadImage("base.png");
  
  b1 = new Base(0, height/2 + 200, base);
  
  minim = new Minim(this);
  in = minim.getLineIn();
  
  gs = new GameState("START");
  playButton1 = loadImage("playButton1.png");
  playButton2 = loadImage("playButton2.png");
  startGame_playButton = new Button(width/2 - startGame_h, (2*height/4) + 180  - startGame_w, playButton1, playButton2);
  restartGame_playButton = new Button(width/2 - startGame_h, (2*height/4) + 150  - startGame_w, playButton1, playButton2);
  
  menuButton1 = loadImage("menuButton1.png");
  menuButton2 = loadImage("menuButton2.png");
  restartGame_menuButton = new Button(width/2 - startGame_h, (3*height/4) + 80 - startGame_w, menuButton1, menuButton2);
  
  infoFg = loadImage("infoFg.png");
  infoButton1 = loadImage("infoButton1.png");
  infoButton2 = loadImage("infoButton2.png");
  startGame_infoButton = new Button(width/2 - startGame_h, (3*height/4) + 110 - startGame_w, infoButton1, infoButton2);
  
  backButton1 = loadImage("backButton1.png");
  backButton2 = loadImage("backButton2.png");
  infoGame_backButton = new Button(width/2 - startGame_h, (3*height/4) + 80 - startGame_w, backButton1, backButton2);

  for (int i = 0; i < infoGame_radioGameModeButtons.length; i++) {
    int y = 170 + i*34;
    infoGame_radioGameModeButtons[i] = new Radio(width/2 - 130, y, 15, color(255), color(252, 145, 0), i, infoGame_radioGameModeButtons);
  }
  for (int i = 0; i < infoGame_radioCharacterSelectButtons.length; i++) {
    int x = 130 + i*60;
    infoGame_radioCharacterSelectButtons[i] = new Radio(x, 3*height/4 - 10, 15, color(255), color(252, 145, 0), i, infoGame_radioCharacterSelectButtons);
  }
  
  gameover = loadImage("gameover.png");
  
  font = createFont("PressStart2P-Regular.ttf", 10);
  textFont(font);
  
  player = minim.loadFile("sfx_wing.mp3");
  point = minim.loadFile("sfx_point.mp3");
  hit = minim.loadFile("sfx_hit.mp3");
  
  mute_icon = loadImage("mute.png");
  sound_icon = loadImage("soundon.png");
  
  String[] lines = loadStrings("cookies.txt");
  highscore = int(lines[0]);
  if (int(lines[1]) == 0){mute = false;}
  else{mute = true;}
}

void draw() {
  if (gs.gameState == "START") {
    gs.startGame();
  }
  else if (gs.gameState == "INFO") {
    gs.infoGame();
  }
  else if (gs.gameState == "PLAY") {
    if (gameMode == 1) {
      println(in.mix.get(0));
      audioVelocity();
    }
    int return_value = gs.playGame();
    if (return_value == 1 && !mute){point.rewind(); point.play();}
    else if (return_value == 2 && !mute){hit.rewind(); hit.play();}
  }
  else if (gs.gameState == "LOSE") {
    highscore = gs.loseGame();
    
  }
  else if (gs.gameState == "RESET"){
    gs.resetGame();
  }
  
  if (mute){image(mute_icon, 440, 10, 50, 50);}
  else{image(sound_icon, 440, 10, 50, 50);}
  
  output = createWriter(dataPath("cookies.txt"));
  output.println(highscore);
  if (mute){output.println(1);}
  else{output.println(0);}
  output.flush();
  output.close();
}

void keyPressed(){
  if (key == ' ' && gs.gameState == "PLAY"){
    gs.spacebarText_y = -100;
    pipe1.velocity = 2;
    pipe2.velocity = 2;
    pipe3.velocity = 2;
    pipe4.velocity = 2;
    bird.gravity = 0.7;
    bird.setVelocity(-10);
    if (!mute){
      player.rewind();
      player.play();
    }
  }
  
  if (key == 'm' || key == 'M'){mute = !mute; print("M"); print(mute);}
}

void mousePressed(){
  if (mouseX > 440 && mouseY < 60){mute = !mute;}
}

void audioVelocity(){
  if (in.mix.get(0) > 0.01){
    bird.setVelocity(in.mix.get(0) * -70);
  }
}
