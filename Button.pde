class Button {
  int x, y;
  PImage img, img2;
  boolean isMouseOver, isMousePressed;
  
  Button(int x, int y, PImage img, PImage img2) {
    this.x = x;
    this.y = y;
    this.img = img;
    this.img2 = img2;
  }
  
  void display() {
    imageMode(CORNER);
    if (isMouseOver) {
      image(img2, x, y);
    } else {
      image(img, x, y);
    }
  }
  
  void update(int mx, int my) {
    if(mx > x && mx < x + startGame_w && my > y && my < y + startGame_h) {
      isMouseOver = true;
    }
    else {
      isMouseOver = false;
    }
  }
  
  boolean isPressed() {
    if (isMouseOver) {
      isMousePressed = true;
      return true;
    }
    return false;
  }
  
  boolean isReleased() {
    isMousePressed = false;
    return false;
  }
  
}
