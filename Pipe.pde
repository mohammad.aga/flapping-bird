class Pipe {
  int pipe_size = 50;
  float gap_size = pipe_size * 3;
  int velocity = 0;
  
  int gap;
  int x;
  boolean passed;
  
  Pipe(int gap, int x){
    this.gap = gap;
    this.x = x;
    passed = false;
  }
  
  void display(){
    stroke(0);
    fill(color(252, 145, 0));
    rect(x, gap - height, pipe_size, height);
    rect(x, gap + gap_size, pipe_size, height);
  }
  
  boolean move(){
    x -= velocity;
    if (x < -pipe_size){
      x = 750;
      passed = false;
      gap = int(random(50, height - 50 - gap_size));
    }
    
    if (x < 50 && !passed){passed = true; return true;}
    
    return false;
  }
}
