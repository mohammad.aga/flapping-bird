class Bird {
  float gravity = 0.0;
  float pipe_size = 50;
  float gap_size = pipe_size * 3;
  float diameter = 25;
  
  float y;
  float velocity;
  float rotation;
  
  PImage bird1;
  PImage bird2;
  PImage bird3;
  PImage bird4;
  PImage bird5;
  
  PImage wing;
  
  Bird (float y, float velocity){
    this.y = y;
    this.velocity = velocity;
    this.rotation = 0;
    bird1 = loadImage("bird.png");
    bird2 = loadImage("bird1.png");
    bird3 = loadImage("bird2.png");
    bird4 = loadImage("bird3.png");
    bird5 = loadImage("bird4.png");
    wing = loadImage("wing.png");
  }
  
  void display() {
    
    if (characterNum == 1) {
      pushMatrix();
      translate(100 - diameter/2, y - (15/34) * diameter);
    
      //draw the bird1 body
      pushMatrix();
      bird1.resize(int(diameter), 0);
      image(bird1, 0, 0);
      popMatrix();
    }
    else if (characterNum == 2) {
      pushMatrix();
      translate(100 - diameter/2, y - (15/34) * diameter);
    
      //draw the bird2 body
      pushMatrix();
      bird2.resize(int(diameter), 0);
      image(bird2, 0, 0);
      popMatrix();
    }
    else if (characterNum == 3) {
      pushMatrix();
      translate(100 - diameter/2, y - (15/34) * diameter);
    
      //draw the bird3 body
      pushMatrix();
      bird3.resize(int(diameter), 0);
      image(bird3, 0, 0);
      popMatrix();
    }
    else if (characterNum == 4) {
      pushMatrix();
      translate(100 - diameter/2, y - (15/34) * diameter);
    
      //draw the bird4 body
      pushMatrix();
      bird4.resize(int(diameter), 0);
      image(bird4, 0, 0);
      popMatrix();
    }
    else if (characterNum == 5) {
      pushMatrix();
      translate(100 - diameter/2, y - (15/34) * diameter);
    
      //draw the bird5 body
      pushMatrix();
      bird5.resize(int(diameter), 0);
      image(bird5, 0, 0);
      popMatrix();
    }
    
    //draw and animate the wing
    pushMatrix();
    wing.resize(int(5*diameter/17), int(4*diameter/17));
    translate(1*diameter/17, 4*diameter/17);
    
    if (velocity<0) {
      rotate(PI/4);
    }
    image(wing,0,0);
    popMatrix();
    popMatrix();
  }
  
  void setVelocity(float vel){
    velocity = vel;
  }
  
  void move(){
    y += velocity;
    velocity += gravity;
  }
  
  boolean checkCollision(ArrayList<Pipe> pipes){
    for (Pipe pipe: pipes){
      if (pipe.x <= 100 && pipe.x + pipe_size >= 100){
        if (y <= pipe.gap + diameter / 2 || y >= pipe.gap + gap_size - diameter / 2){
          return true;
        }
      }
      else if (pipe.x > 100 + diameter / 2 - 1 && pipe.x < 100 + diameter / 2 + 1){
        if (y < pipe.gap || y > pipe.gap + gap_size){return true;}
      }
      else{
        ArrayList<PVector> corners = new ArrayList<PVector>();
        corners.add(new PVector(pipe.x, pipe.gap));
        corners.add(new PVector(pipe.x + pipe_size, pipe.gap));
        corners.add(new PVector(pipe.x, pipe.gap + gap_size));
        corners.add(new PVector(pipe.x + pipe_size, pipe.gap + gap_size));
        
        for (PVector corner : corners){
          if (dist(corner.x, corner.y, 100, y) < diameter / 2){return true;}
        }
      }
    }
    
    return false;
  }
}
