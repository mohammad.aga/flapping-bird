class Base {
  int size = 1344;
  int x, y;
  PImage img;
  
  Base(int x, int y, PImage img){
    this.x = x;
    this.y = y;
    this.img = img;
  }
  
  void display(){
    imageMode(CORNER);
    image(img, x, y);
  }
  
  void move(){
    x -= 2;
    if (x < -size/2){
      x = 0;
    }
  }
}
