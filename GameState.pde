import ddf.minim.*;
import processing.sound.*;

class GameState {
  String gameState;
  int score = 0;
  String[] lines = loadStrings("cookies.txt");
  int highscore = int(lines[0]);
  int spacebarText_y = height/3;
  color gameModeKText_fill = color(252, 145, 0);
  color gameModeAText_fill = color(255);
  
  GameState(String gameState){
    this.gameState = gameState;
  }
  
  void startGame() {
    score = 0;
    imageMode(CORNER);
    image(bg, 0, 0);
    imageMode(CENTER);
    image(title, width/2, height/3);
    startGame_playButton.update(mouseX, mouseY);
    startGame_playButton.display();
    startGame_infoButton.update(mouseX, mouseY);
    startGame_infoButton.display();
    if (mousePressed) {
      if (startGame_playButton.isPressed() ){
        gameState = "PLAY";
      }
      if (startGame_infoButton.isPressed() ){
        gameState = "INFO";
      }
    }
    
  }
  
  void infoGame() {
    imageMode(CORNER);
    image(bg, 0, 0);
    imageMode(CENTER);
    image(infoFg, width/2, height/2);
    textAlign(CENTER);
    textSize(24);
    fill(255);
    text("SETTINGS", width/2, height/4);
    infoGame_backButton.update(mouseX, mouseY);
    infoGame_backButton.display();
    if (mousePressed) {
      if (infoGame_backButton.isPressed() ){
        gameState = "START";
      }
    }
    for (Radio r : infoGame_radioGameModeButtons) {
      r.display();
    }
    for (Radio r2 : infoGame_radioCharacterSelectButtons) {
      r2.display();
    }
    if (mousePressed) {
      for (Radio r: infoGame_radioGameModeButtons) {
        r.isPressed(mouseX, mouseY);
      }
      for (Radio r2: infoGame_radioCharacterSelectButtons) {
        r2.isPressed(mouseX, mouseY);
      }
    }
    
    // Handle GameModes.
    textAlign(LEFT);
    textSize(12);
    fill(gameModeKText_fill);
    text("Keyboard Input Gamemode", width/2 - 110, 177);
    fill(gameModeAText_fill);
    text("Audio Input Gamemode", width/2 - 110, 211);
    if (mousePressed) {
      if (infoGame_radioGameModeButtons[1].isPressed(mouseX, mouseY)) {
        gameModeAText_fill = color(252, 145, 0);
        gameModeKText_fill = color(255);
        gameMode = 1;
      }
      else if (infoGame_radioGameModeButtons[0].isPressed(mouseX, mouseY)) {
        gameModeAText_fill = color(255);
        gameModeKText_fill = color(252, 145, 0);
        gameMode = 0;
      }
    }

    imageMode(CENTER);
    pushMatrix();
    translate(130, height/2 + 50);
    scale(2);
    image(bird1, 0, 0);
    popMatrix();
    pushMatrix();
    translate(190, height/2 + 50);
    scale(2);
    image(bird2, 0, 0);
    popMatrix();
    pushMatrix();
    translate(250, height/2 + 50);
    scale(2);
    image(bird3, 0, 0);
    popMatrix();
    pushMatrix();
    translate(310, height/2 + 50);
    scale(2);
    image(bird4, 0, 0);
    popMatrix();
    pushMatrix();
    translate(370, height/2 + 50);
    scale(2);
    image(bird5, 0, 0);
    popMatrix();
    
    if (mousePressed) {
      if (infoGame_radioCharacterSelectButtons[0].isPressed(mouseX, mouseY)) {
        characterNum = 1;
      }
      if (infoGame_radioCharacterSelectButtons[1].isPressed(mouseX, mouseY)) {
        characterNum = 2;
      }
      if (infoGame_radioCharacterSelectButtons[2].isPressed(mouseX, mouseY)) {
        characterNum = 3;
      }
      if (infoGame_radioCharacterSelectButtons[3].isPressed(mouseX, mouseY)) {
        characterNum = 4;
      }
      if (infoGame_radioCharacterSelectButtons[4].isPressed(mouseX, mouseY)) {
        characterNum = 5;
      }
    }
    
  }
  
  int playGame() {
    int return_value = 0;
    image(bg, 0, 0);
    
    textAlign(CENTER);
    textSize(15);
    fill(255);
    text("Press the spacebar key to start", width/2, spacebarText_y);
    
    pipe1.display();
    if (pipe1.move()){score ++; return_value = 1;}
    pipe2.display();
    if (pipe2.move()){score ++;  return_value = 1;}
    pipe3.display();
    if (pipe3.move()){score ++;  return_value = 1;}
    pipe4.display();
    if (pipe4.move()){score ++;  return_value = 1;}
    
    b1.move();
    b1.display();
  
    bird.move();
    bird.display();
    
    
    textAlign(LEFT);
    textSize(10);
    fill(255);
    text("Score: " + score, 10, 20);
  
    if (bird.checkCollision(pipes)){
      gameState = "LOSE";
      return_value = 2;
    }
    
    // Handle collision between bird and base.
    if(bird.y + bird.diameter >= height/2 + 200){
      gameState = "LOSE";
      return_value = 2;
    }
    
    return return_value;
  }
  
  int loseGame() {
    if (score > highscore){highscore = score;}
    imageMode(CORNER);
    image(bg, 0, 0);
    imageMode(CENTER);
    image(gameover, width/2, height/3);
    textAlign(CENTER);
    fill(255);
    textSize(14);
    text("Highscore: " + highscore, width/2, height/2 - 24);
    text("Your score was: " + score, width/2, height/2 );
    restartGame_playButton.update(mouseX, mouseY);
    restartGame_playButton.display();
    restartGame_menuButton.update(mouseX, mouseY);
    restartGame_menuButton.display();
    if (mousePressed) {
      if (restartGame_playButton.isPressed() ){
        gameState = "RESET";
      }
      if (restartGame_menuButton.isPressed() ){
        pipe1.x = 500;
        pipe2.x = 700;
        pipe3.x = 900;
        pipe4.x = 1100;
        bird.y = 250;
        bird.velocity = 0;
        gameState = "START";
      }
    }
    spacebarText_y = height/3;
    pipe1.velocity = 0;
    pipe2.velocity = 0;
    pipe3.velocity = 0;
    pipe4.velocity = 0;
    bird.gravity = 0;
    bird.setVelocity(0);
    
    return highscore;
  }
  
  void resetGame() {
    score = 0;
    pipe1.x = 500;
    pipe2.x = 700;
    pipe3.x = 900;
    pipe4.x = 1100;
    bird.y = 250;
    bird.velocity = 0;
    gameState = "PLAY";
  }


}
